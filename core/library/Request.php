<?php
	defined("_EXE") or die ("CORE VARIABLE IS NOT DEFINED.");
	class Request extends Palm {

		private $uri = "" ;
		private $service = "page" ;
		private $action = "index";

		public function __construct(){
			$this->parseURI();
		}

		private function parseURI(){
			if($_SERVER["QUERY_STRING"]!==null&&$_SERVER["QUERY_STRING"]!==""){
				$this->uri = $_SERVER["QUERY_STRING"];
				parse_str($_SERVER["QUERY_STRING"], $outputs);
				$this->setService(isset($outputs["com"])?$outputs["com"]:$this->getService());
				$this->setACtion(isset($outputs["action"])?$outputs["action"]:$this->getAction());
				unset($outputs['com']);
				unset($outputs['action']);
				$this->set( isset($_POST)&&!empty($_POST)?array_merge($outputs,$_POST):$outputs ) ;
			}
			else{

				/*
				 * localhost
				 * localhost/
				 * localhost/index.php
				 * localhost/index.php/
				 * localhost/index.php/service
				 * localhost/index.php/service/index
				 * localhost/index.php/service/index/id:1
				 */
				$this->uri = $_SERVER["REQUEST_URI"];
				$vars = array();
				$len=strpos($_SERVER["REQUEST_URI"],"index.php");
				if($len!==false&&strlen($_SERVER["REQUEST_URI"])>($len+10)){
					$outputs = explode("/",substr($_SERVER["REQUEST_URI"],$len+10));
					$this->setService(isset($outputs[0])&&$outputs[0]!==""?$outputs[0]:$this->getService());
					$this->setAction(isset($outputs[1])&&$outputs[1]!==""?$outputs[1]:$this->getAction());
					unset($outputs[0]);
					unset($outputs[1]);

					foreach($outputs AS $output ){
						if(strpos($output,":")!==false){
							list($key,$val)=explode(":",$output);
							$vars = array_merge($vars,array($key=>urldecode($val)));
						}
					}
				}
				isset($_POST)&&!empty($_POST)?$vars=array_merge($vars,$_POST):false;
				$this->set($vars);
			}
		}

		public function setURI($uri){
			$this->uri = $uri ;
			$this->parseURI();
		}

		public function getURI(){
			return $this->uri ;
		}

		public function setService($service){
			$this->service = $service ;
		}

		public function setAction($action){
			$this->action = $action ;
		}

		public function getService(){
			return $this->service;
		}

		public function getAction(){
			return $this->action ;
		}

	}
