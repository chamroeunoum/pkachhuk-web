<?php $books=array(1=>"ក្រមរដ្ឋប្បវេណី",2=>"ក្រមនីតិវិធីរដ្ឋប្បវេណី",3=>"ក្រមព្រហ្មទណ្ឌ",4=>"ក្រមនីតិវិធីព្រហ្មទណ្ឌ"); ?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=1124" />
    <!-- Facebook Share Open Graph Meta Tag -->
    <meta property="fb:app_id" content="454686548045842" />
    <meta property="og:type" content="article" />
    <meta property="article:publisher" content="https://www.facebook.com/khmerlawfiles" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="km_KH" />
    <meta property="og:title" content="យល់ដឹងអំពីច្បាប់" />
    <meta property="og:site_name" content="ផ្កាឈូក" />
    <meta property="og:url" content="<?php echo SERVER.$this->getPath(array("page"=>"articleDetails"),array("bid"=>$bid,"id"=>$id)); ?>" />
    <meta property="og:description" content="<?php echo $books[$bid]." ៖ ".$article->aid." ._ ".$article->title." , អត្ថន័យ ៖ ".substr($article->meaning,0,1000)." ... "; ?>" />
    <meta property="og:image" content="<?php echo Import::link("img/pk-256.png"); ?>" />
    <!-- Facebook Share Open Graph Meta Tag -->
    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ព័ត៌មានលំអិតនៃមាត្រា : ផ្កាឈូក</title>
    <?php
    Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","book01"));
    Import::js(array("jquery","jqueryui","printelementmin","selectize","script","book01"));
    ?>
    <script type="text/javascript" >

        $(document).ready(function(){
            function searchArticle(){
                window.location.href="<?php echo $this->getPath(array("page"=>"search")); ?>/s:"+$(".sec_searchbox").val();
            }
            $(".sec_searchbox").keydown(function(e){
                if(e.keyCode==13){
                    searchArticle();
                }
            });
            $(".sec_btnsearch").click(function(){
                searchArticle();
            });
            $(".fa-home").click(function(){
                window.location.href="<?php echo $this->getPath(array()); ?>";
            });

            $(".go-to-readbook").click(function(){
                window.location.href="<?php echo $this->getPath(array("page"=>"readBook")); ?>";
            });

            $(".go-to-searcharticle").click(function(){
                window.location.href="<?php echo $this->getPath(array("page"=>"search")); ?>";
            });

        });
    </script>
</head>
<body style="background: #FAFAFA;" >

    <!-- Facebook SDK -->

    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '454686548045842',
                xfbml      : true,
                version    : 'v2.3'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <!-- Facebook SDK-->
    <div class="pk-blk pk-mcntn" style="margin: 50px 0px 0px; " >
        <?php echo $this->block("read-article",array("article"=>$article)); ?>
    </div>
    <div class="pk-blk " style="text-align: center; margin: 30px auto;" >
        <?php foreach($relatedArticles AS $id => $art ){ ?>
        <div class="related-article" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"articleDetails"),array("bid"=>$article->bid->id,"id"=>$art->id)); ?>'; " ><?php echo $art->aid . " : " . $art->title; ?></div>
        <?php } ?>
    </div>
    <div class="pk-blk " style="text-align: center; margin: 30px auto;" >
        <div class="book-thumb" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>1)); ?>'; " ><?php echo $books[1]; ?></div>
        <div class="book-thumb" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>2)); ?>'; " ><?php echo $books[2]; ?></div>
        <div class="book-thumb" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>3)); ?>'; " ><?php echo $books[3]; ?></div>
        <div class="book-thumb" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>4)); ?>'; " ><?php echo $books[4]; ?></div>
    </div>
    <div class="pk-ft" style="display: none;" >
        <div class="pk-purpose" >សម្រាប់ការប្រើប្រាស់ជាសាធារណៈ<div style="position: relative; float: right; display: inline-block; top: -3px; padding: 0px 10px; cursor: pointer; color: #02508c; " >អំពី<span style="font-size:16px; color: #D770AD; font-weight: bold" >ខ្ញុំ</span></div></div>
    </div>
    <div class="pk-blk" style=" border-botto: 1px solid #CCC; box-shadow: 0px 0px 2px #AAA; height: 40px; padding: 5px 0px 4px 0px; position: fixed; left: 0; top: 0; float: left; background: #FAFAFA; " >
        <div class="pk-logo" style=" border:none; border-bottom: 1px solid #CCC;" ></div>
    </div>
    <div class="fa fa-home" style="position: absolute; cursor: pointer; font-size: 30px; left: 0; top: 5px; color: #333; width: 50px; height: 40px; line-height: 40px; text-align: center;  " ></div>
    <div class="fa fa-book go-to-readbook" style="position: fixed; top: 10px; right: 45px; height: 30px; width: 30px; line-height: 30px; text-align: center; font-size: 20px; cursor: pointer; background: #178ee9; border-radius: 15px ; color: #FAFAFA;  " title="អានឯកសារច្បាប់ផ្សេងៗ" ></div>
    <div class="fa fa-search go-to-searcharticle" style="position: fixed; top: 10px; right: 5px; height: 30px; width: 30px; line-height: 30px; text-align: center; font-size: 20px; cursor: pointer; background: #178ee9; border-radius: 15px ; color: #FAFAFA;  " title="ស្វែងរកមាត្រាទាំងអស់នៃឯកសារច្បាប់ក្នុងប្រព័ន្ធ" ></div>
</body>
</html>