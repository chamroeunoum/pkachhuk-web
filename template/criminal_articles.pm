<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ទំព័រដើម : ផ្កាឈូក</title>
    <?php
        Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","home","main"));
        Import::js(array("jquery","jqueryui","printelementmin","selectize","script","home"));
    ?>
</head>
<body>
    <div class="pk-bd" >
        <div class="pk-blk-center" >
            <div class="book-item" onclick="window.location='<?php echo $this->getPath(array("civil"=>"index")); ?>';" >ច្បាប់<br/> ព្រហ្មទណ្ឌ</div>
        </div>
        <div class="index_2_728x90" >
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Index_2_728x90 -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-4781007779447186"
                 data-ad-slot="8488083952"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <div class="pk-blk" >
            <div class="pk-blk navigation" >
                <div class="nav-prev fa fa-arrow-left" onclick="window.location='<?php echo $this->getPath(array("criminal"=>"index"),array("p"=>$prev)); ?>';" ></div>
                <div class="nav-info" >ទំព័រ <?php echo $current; ?> នៃ <?php echo $total_pages; ?></div>
                <div class="nav-next fa fa-arrow-right" onclick="window.location='<?php echo $this->getPath(array("criminal"=>"index"),array("p"=>$next)); ?>';" ></div>
            </div>
            <div class="pk-search-message" >កម្រងមាត្រាទាំងអស់ (<?php echo $total_articles; ?>) ៖ </div>
            <div class="pk-search-result" >
                <?php foreach( $articles AS $article_id => $article_content ){ ?>
                <div class="pk-search-item" >
                    <div class="article_id" >មាត្រា <?php echo $article_id; ?> ៖ </div>
                    <div class="article_cntn cut_string " style="font-family: LMNS1; font-size: 26px; " ><?php echo $article_content; ?></div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="index_2_728x90" >
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Index_2_728x90 -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-4781007779447186"
                 data-ad-slot="8488083952"
                 data-ad-format="auto"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
    <div class="pk-ft" >
        <div class="pk-purpose" >សម្រាប់ការប្រើប្រាស់ជាសាធារណៈ</div>
    </div>
    <div class="pk-mmenu-opener fa fa-reorder" ></div>
    <div class="pk-mmenu" >
        <div class="pk-slogo cut_string" onclick="window.location='<?php echo $this->getPath(array("page"=>"index")); ?>';" ></div>
        <div class="pk-mmenu-closer fa fa-arrow-left " style="text-indent: 0px;" ></div>
        <div class="pk-prdc-scn pk-blk" >
            <div class="book-item" ><a href="<?php echo $this->getPath(array("criminal"=>"index")); ?>" class="<?php echo $mnu=="criminal"?" active":""; ?>" >ច្បាប់<br/> ព្រហ្មទណ្ឌ</a></div>
            <div class="book-item" ><a href="<?php echo $this->getPath(array("civil"=>"index")); ?>" class="<?php echo $mnu=="civil"?" active":""; ?>" >ច្បាប់<br/> រដ្ឋប្បវេណី</a></div>
            <div class="book-item" style="color: #AAA;" >ច្បាប់<br/>ភូមិបាល</div>
            <div class="book-item" style="color: #AAA;" >និតិ<br/>រដ្ឋប្បវេណី</div>
            <div class="book-item" style="color: #AAA;" >និតិ<br/>ព្រហ្មទណ្ឌ</div>
        </div>
    </div>
</body>
</html>