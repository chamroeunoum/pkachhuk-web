<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ទំព័រដើម : ផ្កាឈូក</title>
    <?php
    Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","book"));
    Import::js(array("jquery","jqueryui","printelementmin","selectize","script","book"));
    ?>
    <script type="text/javascript" >

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1011781982167975',
                xfbml      : true,
                version    : 'v2.0'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $(document).ready(function(){
            $(".pk-book-item").click(function(){
                var me = this;
                $.ajax({
                    url:"<?php echo $this->getPath(array("page"=>"getArticlesByCategory")); ?>",
                    type:"POST",
                    data:{id:$(me).attr("value")},
                    beforeSend:function(){
                        $(me).parent().parent().parent().find(".pk-matra-list").html("<span class='loading-article' >កំពុងអានមាត្រា...</span>");
                    },
                    success:function(response){
                        $(me).parent().parent().parent().find(".pk-matra-list").hide().html(response).slideDown();
                    }
                });
            });
            $(".back-home").click(function(){
                window.location.href="<?php echo $this->getPath(array()); ?>";
            });
            $(".fa-arrow-circle-o-down").click(function(){
                $(".list-books").css("display")!="none"?$(".list-books").slideUp():$(".list-books").slideDown();
            });
            $(".fa-arrow-left").click(function(){
                window.location.href='<?php echo $this->getPath(array("page"=>"index")); ?>';
            });
            $(".go-to-searcharticle").click(function(){
                window.location.href="<?php echo $this->getPath(array("page"=>"search")); ?>";
            });
        });
    </script>
</head>
<body>
    <div class=" pk-mcntn" >
        <div class="pk-mcntn-right" >
            <div class="pk-blk pk-matra-list" ></div>
        </div>
        <div class="pk-mcntn-left" >
            <?php echo $this->block("book-content",array("book"=>$book,"bookid"=>$bid)); ?>
        </div>
    </div>
    <div class="pk-ft" >
        <div class="pk-purpose" >អ៊ីមែលអ្នកគ្រប់គ្រងគេហទំព័រ៖ admin@pkachhuk.org ឬ info@pkachhuk.org</div>
    </div>
    <div class="pk-blk" style=" box-shadow: 0px 0px 2px #AAA; height: 40px; padding: 5px 0px 4px 0px; position: fixed; left: 0; top: 0; float: left; background: #FAFAFA; " >
        <div class="pk-logo" style=" border:none; border-bottom: 1px solid #CCC;" ></div>
        <div class="book-title" style="color: #009867;" ><?php $books=array(1=>"ក្រមរដ្ឋប្បវេណី",2=>"ក្រមនីតិវិធីរដ្ឋប្បវេណី",3=>"ក្រមព្រហ្មទណ្ឌ",4=>"ក្រមនីតិវិធីព្រហ្មទណ្ឌ"); echo $books[$bid]; ?>
            <div class="list-books" >
                <div class="book-item" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>1)); ?>';" >ក្រមរដ្ឋប្បវេណី</div>
                <div class="book-item" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>2)); ?>';" >ក្រមនីតិវិធីរដ្ឋប្បវេណី</div>
                <div class="book-item" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>3)); ?>';" >ក្រមព្រហ្មទណ្ឌ</div>
                <div class="book-item" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>4)); ?>';" >ក្រមនីតិវិធីព្រហ្មទណ្ឌ</div>
            </div>
        </div>
        <div class="fa fa-arrow-circle-o-down" style="position: absolute; cursor: pointer; font-size: 30px; left: 25%; top: 5px; color: #009867; width: 40px; height: 40px; line-height: 40px; text-align: center;  " ></div>
        <div class="fa fa-home" style="position: absolute; cursor: pointer; font-size: 30px; left: 0; top: 5px; color: #333; width: 50px; height: 40px; line-height: 40px; text-align: center;  " ></div>
        <div class="fa fa-search go-to-searcharticle" style="position: fixed; top: 10px; right: 5px; height: 30px; width: 30px; line-height: 30px; text-align: center; font-size: 20px; cursor: pointer; background: #178ee9; border-radius: 15px ; color: #FAFAFA;  " title="ស្វែងរកមាត្រាទាំងអស់នៃឯកសារច្បាប់ក្នុងប្រព័ន្ធ" ></div>
    </div>
</body>
</html>