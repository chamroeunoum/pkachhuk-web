<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ទំព័រដើម : ផ្កាឈូក</title>
    <?php
        Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","home"));
        Import::js(array("jquery","jqueryui","printelementmin","selectize","script","home"));
    ?>
</head>
<body>
    <div class="pk-bd" >
        <div class="pk-blk" style="text-align: center;" ><div class="pk-blogo" >&nbsp;</div></div>
        <div class="pk-blk" >
            <div class="pk-blk-center" style="width: 600px; " ><?php HTML::control("ios7/SearchBox"); ?></div>
        </div>
        <div class="pk-blk" >
            <div class="pk-search-message" >លទ្ធផលនៃការស្វែងរក <span style="color: rgb(192,68,142); " >ការអភិរក្សព្រៃឈើ</span> ៖ </div>
            <div class="pk-search-result" >
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
                <div class="pk-search-item" ></div>
            </div>
            <div class="pk-search-loader" >បង្ហាញបន្ថែមទៀត (283)</div>
        </div>
    </div>
    <div class="pk-ft" >
        <div class="pk-purpose" >មិនគិតថ្លៃ!!! សំរាប់អ្នកជារៀងរហូត!!!</div>
    </div>
    <div class="pk-mmenu-opener fa fa-reorder" ></div>
    <div class="pk-mmenu" >
        <div class="pk-slogo cut_string" ><div class="pk-mmenu-closer fa fa-arrow-left " style="text-indent: 0px;" ></div></div>
        <div class="pk-prdc-scn pk-blk" >
            <div class="pk-prdc" ><a href="#pumibal" class="cut_string<?php echo $mnu=="pumibal"?" active":""; ?>" > ច្បាប់ភូមិបាល</a></div>
            <div class="pk-prdc" ><a href="#promiton" class="cut_string<?php echo $mnu=="promiton"?" active":""; ?>" >ច្បាប់ព្រហ្មទណ្ឌ</a></div>
            <div class="pk-prdc" ><a href="#rodpavenii" class="cut_string<?php echo $mnu=="rodpavenii"?" active":""; ?>" >ច្បាប់រដ្ឋប្បវេណី</a></div>
        </div>
    </div>
</body>
</html>