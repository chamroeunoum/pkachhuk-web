<div class="pk-blk pk-matra-item" style="width: 97%;" >
    <div class="pk-matra-book-title" style="position: relative; display: block; width: 100%; padding: 10px 0px 20px; text-align: center; font-family: KHMUOL; color: #B04A6F; font-size: 20px; " ><?php echo $article->bid->title; ?></div>
    <div class="pk-matra-info" ><?php
        $layout = array();
        is_object($article->kid)?$layout[] = $article->kid->kid." ៖ ".$article->kid->title:"";
        is_object($article->mid)?$layout[] = $article->mid->mid." ៖ ".$article->mid->title:"";
        is_object($article->cid)?$layout[] = $article->cid->cid." ៖ ".$article->cid->title:"";
        is_object($article->pid)?$layout[] = $article->pid->pid." ៖ ".$article->pid->title:"";
        is_object($article->sid)?$layout[] = $article->sid->sid." ៖ ".$article->sid->title:"";
        echo implode(" > " , array_filter($layout));
    ?></div>
    <div class="pk-matra-item-no" ><?php echo $article->aid; ?>&nbsp;._&nbsp;</div>
    <div class="pk-matra-item-title" ><?php echo $article->title; ?></div>
    <div class="pk-matra-item-cntn" style="width: 100%; border: none; text-align: justify; padding: 20px 0px; " ><?php echo $article->meaning; ?></div>
    <div
        class="fb-share-button"
        data-layout="button_count"
        data-share="true"
        data-width="450"
        data-show-faces="false"
        style="right: 45px; position: absolute; top: 99px;" >
    </div>
</div>