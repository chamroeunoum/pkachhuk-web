<div class="pk-book-cntn" >
    <?php
    foreach( $book["kunties"] as $kid => $kunty ){ ?>
        <div class="pk-book-item cut-string" value="<?php echo $bookid; ?>,<?php echo $kunty["id"]; ?>" ><?php echo "<span class='pk-book-cntn-kunty' ></span>".$kunty["kid"]." : ".$kunty["title"]; ?></div>
        <?php
        if(isset($book["kunties"][$kid]["matikas"])){
            foreach($book["kunties"][$kid]["matikas"] AS $mid => $matika ){ ?>
                <div class="pk-book-item cut-string" title="<?php echo $matika["title"]; ?>" value="<?php echo $bookid; ?>,<?php echo $kunty["id"]; ?>,<?php echo $matika["id"]; ?>" ><?php echo "&nbsp;&nbsp;<span class='pk-book-cntn-matika' style='left: 20px; ' ></span>".$matika["mid"]." : ".$matika["title"]; ?></div>
                <?php
                if(isset($book["kunties"][$kid]["matikas"][$mid]["chapters"])){
                    foreach($book["kunties"][$kid]["matikas"][$mid]["chapters"] AS $cid => $chapter ){ ?>
                        <div class="pk-book-item cut-string" title="<?php echo $chapter["title"]; ?>" value="<?php echo $bookid; ?>,<?php echo $kunty["id"]; ?>,<?php echo $matika["id"]; ?>,<?php echo $chapter["id"]; ?>" ><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;<span class='pk-book-cntn-chapter'  style='left: 30px; ' ></span>".$chapter["cid"]." : ".$chapter["title"]; ?></div>
                        <?php
                        if(isset($book["kunties"][$kid]["matikas"][$mid]["chapters"][$cid]["parts"])) {
                            foreach ($book["kunties"][$kid]["matikas"][$mid]["chapters"][$cid]["parts"] AS $pid => $part) { ?>
                                <div class="pk-book-item cut-string"
                                     title="<?php echo $part["title"]; ?>"
                                     value="<?php echo $bookid; ?>,<?php echo $kunty["id"]; ?>,<?php echo $matika["id"]; ?>,<?php echo $chapter["id"]; ?>,<?php echo $part["id"]; ?>"><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='pk-book-cntn-part' style='left: 40px; ' ></span>". $part["pid"] . " : " . $part["title"]; ?></div>
                                <?php
                                if(isset($book["kunties"][$kid]["matikas"][$mid]["chapters"][$cid]["parts"][$pid]["sections"])){
                                    foreach($book["kunties"][$kid]["matikas"][$mid]["chapters"][$cid]["parts"][$pid]["sections"] AS $sid => $section ){ ?>
                                        <div class="pk-book-item cut-string" title="<?php echo $section["title"]; ?>" value="<?php echo $bookid; ?>,<?php echo $kunty["id"]; ?>,<?php echo $matika["id"]; ?>,<?php echo $chapter["id"]; ?>,<?php echo $part["id"]; ?>,<?php echo $section["id"]; ?>" ><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span  class='pk-book-cntn-section' style='left: 50px; ' ></span>".$section["sid"]." : ".$section["title"]; ?></div>
                                    <?php }
                                }
                            }
                        }
                    }
                }
            }
        }else if(isset($book["kunties"][$kid]["chapters"])){
            foreach($book["kunties"][$kid]["chapters"] AS $cid => $chapter ){ ?>
                <div class="pk-book-item cut-string" title="<?php echo $chapter["title"]; ?>" value="<?php echo $bookid; ?>,<?php echo $kunty["id"]; ?>,0,<?php echo $chapter["id"]; ?>" ><?php echo "&nbsp;&nbsp;<span class='pk-book-cntn-chapter' style='left: 20px; ' ></span>".$chapter["cid"]." : ".$chapter["title"]; ?></div>
                <?php
                if(isset($book["kunties"][$kid]["chapters"][$cid]["parts"])){
                    foreach($book["kunties"][$kid]["chapters"][$cid]["parts"] AS $pid => $part ){ ?>
                        <div class="pk-book-item cut-string" title="<?php echo $part["title"]; ?>" value="<?php echo $bookid; ?>,<?php echo $kunty["id"]; ?>,0,<?php echo $chapter["id"]; ?>,<?php echo $part["id"]; ?>" ><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;<span class='pk-book-cntn-part' style='left: 30px; '  ></span>".$part["pid"]." : ".$part["title"]; ?></div>
                        <?php
                        if(isset($book["kunties"][$kid]["chapters"][$cid]["parts"][$pid]["sections"])){
                            foreach($book["kunties"][$kid]["chapters"][$cid]["parts"][$pid]["sections"] AS $sid => $section ){ ?>
                                <div class="pk-book-item cut-string" title="<?php echo $section["title"]; ?>" value="<?php echo $bookid; ?>,<?php echo $kunty["id"]; ?>,0,<?php echo $chapter["id"]; ?>,<?php echo $part["id"]; ?>,<?php echo $section["id"]; ?>" ><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='pk-book-cntn-section' style='left: 40px; '  ></span>".$section["sid"]." : ".$section["title"]; ?></div>
                            <?php }
                        }
                    }
                }
            }
        }
    } ?>
</div>