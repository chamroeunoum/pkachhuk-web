<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=1124" />

    <!-- Facebook Share Open Graph Meta Tag -->
    <meta property="fb:app_id" content="454686548045842" />
    <meta property="og:type" content="article" />
    <meta property="article:publisher" content="https://www.facebook.com/khmerlawfiles" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="km_KH" />
    <meta property="og:title" content="យល់ដឹងអំពីច្បាប់" />
    <meta property="og:site_name" content="ផ្កាឈូក" />
    <meta property="og:url" content="<?php echo SERVER.$this->getPath(array("page"=>"articleDetails"),array("bid"=>$bid,"id"=>$id)); ?>" />
    <meta property="og:description" content="<?php echo $books[$bid]." ៖ ".$article->aid." ._ ".$article->title."<br/><br/>".substr($article->meaning,0,1000)." ... "; ?>" />
    <meta property="og:image" content="<?php echo Import::link("img/pk-256.png"); ?>" />
    <!-- Facebook Share Open Graph Meta Tag -->

    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ទំព័រដើម : ផ្កាឈូក</title>
    <?php
    Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","book02"));
    Import::js(array("jquery","jqueryui","printelementmin","selectize","script","book01"));
    ?>
    <script type="text/javascript" >

        $(document).ready(function(){

            $(".search-helper").click(function(){
                $(".cntn-overlay").show();
                $(".msg-cntn").html("<span style='display: block; width: 100%; text-align: center; font-family: KHMUOL; font-size: 20px; padding: 10px 0px 20px; '>សេចក្ដីណែនាំ</span>សូមបញ្ចូលកម្រងពាក្យដែលអ្នកចង់ស្វែងរកក្នុងប្រអប់ស្វែងរក ដោយដកឃ្លាពីពាក្យមួយទៅពាក្យមួយ ដើម្បីទទួលបានលទ្ធផលនៃការស្វែងរកកាន់តែសុក្រឹត។ សូមអរគុណ !!!<br/><br/>ឧទាហរណ៍ ៖ សិទ្ធិមនុស្ស -> សិទ្ធិ មនុស្ស<br/><br/>ចំណាំ ៖ ក្នុងករណីដែលអ្នកសសេរដូចខាងក្រោម <br/><br/> ១. សិទ្ធិមនុស្ស ៖ ប្រព័ន្ធនិងស្វែងរកមាត្រាដែលមានពាក្យថា សិទ្ធិមនុស្ស<br/><br/>២. សិទ្ធិ មនុស្ស ៖ ប្រព័ន្ធនិងស្វែងរកមាត្រាដែលមានពាក្យ សិទ្ធិ និង មនុស្ស");
                $(".msg-h").slideDown();
            });

            $(".go-to-readbook").click(function(){
                window.location.href="<?php echo $this->getPath(array("page"=>"readBook")); ?>";
            });

            $(".search-item").click(function(){
                var articleId = $(this).attr("id").split(",");
                $.ajax({
                    url:"<?php echo $this->getPath(array("page"=>"readArticle"));?>",
                    type:"POST",
                    data:{
                        id:articleId[1],
                        bid:articleId[0],
                        s:$(".sec_searchbox").val()
                    },
                    success:function(response){
                        $(".cntn-overlay").show();
                        $(".msg-cntn").html(response);
                        FB.XFBML.parse();
                        $(".msg-h").slideDown();
                    }
                });
            });

            function searchArticle(){
                window.location.href="<?php echo $this->getPath(array("page"=>"search")); ?>/s:"+$(".sec_searchbox").val();
            }
            $(".sec_searchbox").keydown(function(e){
                if(e.keyCode==13){
                    searchArticle();
                }
            });
            $(".sec_btnsearch").click(function(){
                searchArticle();
            });

            $(".go-to-search").click(function(){
                window.scrollTo(0,0);
            });

        });
    </script>
</head>
<body style="background: #FAFAFA;" >

<!-- Facebook SDK -->

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '454686548045842',
            xfbml      : true,
            version    : 'v2.3'
        });

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Facebook SDK-->

    <div class="search-result" style="background: rgba(250,250,250,0.4); padding: 20px 1% 5px 1%; width: 98%; margin: 120px auto 10px auto;" >លទ្ធផលនៃការស្វែងរក <span style="color: #9859B6; font-size: 16px;  " ><?php echo count($articles); ?></span> មាត្រា ៖</div>
    <div class="pk-blk pk-search-blk" style="text-align: center; position: relative; float: left; width: 100%; margin: 10px 0px 60px 0px; " >
        <?php
        $books=array(1=>"ក្រមរដ្ឋប្បវេណី",2=>"ក្រមនីតិវិធីរដ្ឋប្បវេណី",3=>"ក្រមព្រហ្មទណ្ឌ",4=>"ក្រមនីតិវិធីព្រហ្មទណ្ឌ");
        foreach( $articles AS $index => $article ){ ?>
        <div class="search-item" id="<?php echo $article->bid.",".$article->id; ?>">
            <div class="matra-no" ><?php echo $article->aid; ?></div>
            <div class="matra-title" ><?php echo $article->title; ?></div>
            <div class="matra-book" ><?php echo isset($books[$article->bid])?$books[$article->bid]:"មិនមានឈ្មោះ"; ?></div>
<!--            <div class="fb-share-button" data-layout="button_count" style="display: block; position: absolute; right: 5px; top: 10px;" data-href="--><?php //echo SERVER.$this->getPath(array("page"=>"articleDetails"),array("bid"=>$article->bid,"id"=>$article->id));?><!--" ></div>-->
        </div>
        <?php } ?>
    </div>
    <div class="pk-ft" >
        <div class="pk-purpose" >អ៊ីមែលអ្នកគ្រប់គ្រងគេហទំព័រ៖ admin@pkachhuk.org ឬ info@pkachhuk.org</div>
    </div>

    <div class="cntn-overlay" ></div>
    <div class="msg-h" >
        <div class="msg-cntn" ></div>
        <div class="msg-btns" >
            <input type="button" value="បិទ" class="msg-cancel" />
            <input type="button" value="បាទ" class="msg-ok" style="display: none; " />
        </div>
    </div>
    <div class="pk-book-search" style="border: 1px solid #DDD; border-radius: 5px; " >
        <div class="sec_search_h" style="display: block; margin: 0px; width: 100%; height: 38px; " >
            <input type="text" style="background: #FAFAFA; color: #333; width: 88%; padding: 4px 1% ; height: 30px; line-height: 30px; " class="sec_searchbox " name="sec_searchbox" value="<?php echo @$search; ?>" placeholder="សូមបញ្ចូល ចំណងជើង ឬ អត្ថន័យ នៃ មាត្រា  ដើម្បីស្វែងរក..." />
            <div class="sec_btnsearch fa fa-search" style="width: 10%; height: 40px; line-height: 40px;  " ></div>
        </div>
    </div>
    <div class="book-title" >ស្វែងរកមាត្រាពីគ្រប់ឯកសារច្បាប់ ៖</div>
    <div class="pk-blk" style=" box-shadow: 0px 0px 2px #AAA; height: 40px; padding: 5px 0px 4px 0px; position: fixed; left: 0; top: 0; float: left; background: #FAFAFA; " >
        <div class="pk-logo" style=" border:none; border-bottom: 1px solid #CCC;" ></div>
        <div class="fa fa-home" onclick="window.location.href='<?php echo $this->getPath(array("page"=>"index")); ?>';" style="position: absolute; cursor: pointer; font-size: 30px; left: 0; top: 5px; color: #333; width: 50px; height: 40px; line-height: 40px; text-align: center;  " ></div>
    </div>
    <div class="fa fa-search go-to-search" style="position: fixed; bottom: 30px; right: 0px; height: 40px; width: 40px; line-height: 40px; text-align: center; font-size: 30px; cursor: pointer; background: #00578d; border-radius: 5px 0px 0px 0px; color: #FAFAFA;  " title="ទៅកាន់ប្រអប់ស្វែងរក" ></div>
    <div class="fa fa-question search-helper" style="position: fixed; top: 10px; right: 5px; height: 30px; width: 30px; line-height: 30px; text-align: center; font-size: 20px; cursor: pointer; background: #178ee9; border-radius: 15px ; color: #FAFAFA;  " title="ការណែនាំសម្រាប់ស្វែងរកមាត្រានែឯកសារច្បាប់ផ្សេងៗ" ></div>
    <div class="fa fa-book go-to-readbook" style="position: fixed; top: 10px; right: 45px; height: 30px; width: 30px; line-height: 30px; text-align: center; font-size: 20px; cursor: pointer; background: #178ee9; border-radius: 15px ; color: #FAFAFA;  " title="អានឯកសារច្បាប់ផ្សេងៗ" ></div>
</body>
</html>