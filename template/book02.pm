<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=1124" />

    <!-- Facebook Share Open Graph Meta Tag -->
    <meta property="fb:app_id" content="454686548045842" />
    <meta property="og:type" content="website" />
    <meta property="article:publisher" content="https://www.facebook.com/khmerlawfiles" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="km_KH" />
    <meta property="og:title" content="គេហទំព័រយល់ដឹងអំពីច្បាប់" />
    <meta property="og:site_name" content="ផ្កាឈូក" />
    <meta property="og:url" content="http://www.pkachhuk.org" />
    <meta property="og:description" content="នេះជាគេហទំព័រ សម្រាប់មិត្តគ្រប់គ្នាយល់ដឹងអំពីច្បាប់ផ្សេង នៃ ព្រះរាជាណាចក្រកម្ពុជា។ សូមស្វែងយល់អំពីឯកសារច្បាប់ផ្សេងៗ ដោយរីករាយ និង សូមអោយគេហទំព័រនេះក្លាយជាប្រយោជន៍ដល់អ្នកគ្រប់គ្នា ក្នុងផ្លូវល្អ ជាពិសេស មិត្ត សិស្សានុសិស្ស។ សូមអរគុណ !!!" />
    <meta property="og:image" content="<?php echo Import::link("img/pk-256.png"); ?>" />
    <!-- Facebook Share Open Graph Meta Tag -->

    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ទំព័រដើម : ផ្កាឈូក</title>
    <?php
    Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","book02"));
    Import::js(array("jquery","jqueryui","printelementmin","selectize","script","book01"));
    ?>
    <script type="text/javascript" >


        $(document).ready(function(){

            function searchArticle(){
                window.location.href="<?php echo $this->getPath(array("page"=>"search")); ?>/s:"+$(".sec_searchbox").val();
            }
            $(".sec_searchbox").keydown(function(e){
                if(e.keyCode==13){
                    searchArticle();
                }
            });
            $(".sec_btnsearch").click(function(){
                searchArticle();
            });

            $(".mitika-shower").click(function(){
                var bcontent = $(this).parent().find(".pk-book-cntn");
                if($(this).parent().find(".pk-book-cntn").css("display")!="none"){
                    $(this).parent().find(".mitika-shower").css("left","0px");
                }else{
                    $(this).parent().find(".mitika-shower").css("left","250px");
                }
                $(this).parent().find(".pk-book-cntn").toggle("slide");
            });

            $(".read-law-book").click(function(){
                window.location.href="<?php echo $this->getPath(array("page"=>"readBook"),array("id"=>1));?>";
            });

            $(".search-all-article").click(function(){
                window.location.href='<?php echo $this->getPath(array("page"=>"search")); ?>';
            });

            $(".visit-on-facebook").click(function(){
                window.location.href="https://www.facebook.com/khmerlawfiles";
            });

        });
    </script>
</head>
<body>

<!-- Facebook SDK -->

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '454686548045842',
            xfbml      : true,
            version    : 'v2.3'
        });

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Facebook SDK-->


    <div class="book-ads" >
        <div class="pk-logo-big" ></div>
        <div class="welcome-message" >គេហទំព័រ ផ្កាឈូក សូមស្វាគមន៍</div>
        <div class="search-all-article" ><div class="fa fa-search search-article" style="display: block;font-size: 80px; color: #009867; margin: 0 0 30px 0; "  ></div>ស្វែងរកមាត្រាគ្រប់ឯកសារច្បាប់</div>
        <div class="read-law-book" ><div class="fa fa-book read-book" style="display: block;font-size: 80px; color: #009867; margin: 0 0 30px 0; " ></div>អានឯកសារច្បាប់</div>
    </div>

    <div class="pk-blk pk-mcntn" >
        <div class="brocher" >
            <div class="brocher-holder" >ក្រមរដ្ឋប្បវេណី</div></div>
            <div class="brocher-body civil-background" style=" position: relative; display: block; float: left; height: 550px; padding: 0px 28% 0px 0%; width: 72%; " >
                <?php echo $this->block("book-content",array("book"=>$civilBook)); ?>
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ក្រមនិតិវិធីរដ្ឋប្បវេណី</div>
            <div class="brocher-body civil-procedure-background" style="height: 550px; padding: 0px 0% 0px 28%; width: 72%; " >
                <?php echo $this->block("book-content",array("book"=>$civilProcedureBook)); ?>
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ក្រមព្រហ្មទណ្ឌ</div>
            <div class="brocher-body criminal-background" style="height: 550px; padding: 0px 28% 0px 0%; width: 72%;" >
                <?php echo $this->block("book-content",array("book"=>$criminalBook)); ?>
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ក្រមនិតិវិធីព្រហ្មទណ្ឌ</div>
            <div class="brocher-body criminal-procedure-background" style="height: 550px; padding: 0px 0% 0px 28%; width: 72%; " >
                <?php echo $this->block("book-content",array("book"=>$criminalProcedureBook)); ?>
            </div>
        </div>

        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់រដ្ឋធម្មនុញ្ញ</div>
            <div class="brocher-body constitution-background" style="height: 300px; padding: 150px 35% 100px 5%; width: 60%; font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់ស្ដីការប្រឆាំងនឹងភេរវកម្ម</div>
            <div class="brocher-body torrorism-background" style="height: 300px; padding: 150px 5% 100px 35%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់សហគ្រាសពាណិជ្ជកម្ម</div>
            <div class="brocher-body commercial-background" style="height: 300px; padding: 150px 35% 100px 5%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់សិទ្ធិអ្នកនិពន្ធ និងសិទ្ធិប្រហាក់ប្រហែល</div>
            <div class="brocher-body author-background" style="height: 300px; padding: 150px 5% 100px 35%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់ព្រៃឈើ</div>
            <div class="brocher-body forestry-background" style="height: 300px; padding: 150px 35% 100px 5%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់ចរាចរណ៍ផ្លូវគោក</div>
            <div class="brocher-body traffic-land-background" style="height: 300px; padding: 150px 5% 100px 35%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់ការគ្រប់គ្រង និងការធ្វើអាជីវកម្មធនធានរ៉ែ </div>
            <div class="brocher-body mining-background" style="height: 300px; padding: 150px 35% 100px 5%; width: 60%;  font-family: KHMUOL; font-size: 20px;"  >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់ការបង្រ្កាបអំពើជួញដូរមនុស្ស និង អំពើធ្វើអាជីវកម្មផ្លូវភេទ</div>
            <div class="brocher-body human-traffic-background" style="height: 300px; padding: 150px 5% 100px 35%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់ស្ដីពីសារពើពន្ធ</div>
            <div class="brocher-body taxation-background" style="height: 300px; padding: 150px 35% 100px 5%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់វិសោធនកម្ម នៃច្បាប់ស្ដីពីសារពើពន្ធ </div>
            <div class="brocher-body taxation-amendment-background" style="height: 300px; padding: 150px 5% 100px 35%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <div class="brocher" >
            <div class="brocher-holder" >ច្បាប់ស្ដីពីអគ្គីសនី នៃព្រះរាជាណាចក្រកម្ពុជា </div>
            <div class="brocher-body electricy-background" style="height: 300px; padding: 150px 35% 100px 5%; width: 60%;  font-family: KHMUOL; font-size: 20px;" >
                <br/><br/>
                <br/><br/>
                កំពុងរៀបចំទិន្ន័យឡើង...
            </div>
        </div>
        <style type="text/css" >
            .pk-book-cntn {
                position: relative;
                display: block;
                float: left;
                width: 100%;
                background: none;
                overflow-y: hidden;
                height: 550px;
            }
            .pk-book-cntn .pk-book-cntn-kunty,.pk-book-cntn .pk-book-cntn-matika, .pk-book-cntn .pk-book-cntn-chapter,.pk-book-cntn .pk-book-cntn-part,.pk-book-cntn .pk-book-cntn-section { display: none;}
            .pk-book-item {
                width: 100%;
                text-align: center;
                color: #FAFAFA;
            }
            .pk-book-item {
                font-family: KHMUOL;
                font-size: 18px;
                padding: 10px 0px;
                cursor: text;
            }
            .pk-book-item:hover {background: none;}
        </style>
    </div>
    <div class="pk-ft" >
        <div class="pk-purpose" >អ៊ីមែលអ្នកគ្រប់គ្រងគេហទំព័រ៖ admin@pkachhuk.org ឬ info@pkachhuk.org</div>
    </div>
    <div class="fa fa-facebook-square visit-on-facebook" style="position: fixed; top: 0px; right: 0px; height: 30px; padding: 10px; line-height: 30px; text-align: center; font-size: 30px; cursor: pointer; background: #FAFAFA; border-radius: 0px 0px 0px 10px ; color: #178ee9;  " title="ជួបជាមួយខ្ញុំនៅ ​Facebook" ></div>
</body>
</html>