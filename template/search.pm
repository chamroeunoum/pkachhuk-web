<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=1124" />
    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ទំព័រដើម : ផ្កាឈូក</title>
    <?php
    Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","book01"));
    Import::js(array("jquery","jqueryui","printelementmin","selectize","script","book01"));
    ?>
    <script type="text/javascript" >
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '454686548045842',
                xfbml      : true,
                version    : 'v2.3'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $(document).ready(function(){
            $(".search-item").click(function(){
                var articleId = $(this).attr("id");
                $.ajax({
                    url:"<?php echo $this->getPath(array("page"=>"readArticle"));?>",
                    type:"POST",
                    data:{
                        id:articleId,
                        s:$(".sec_searchbox").val()
                    },
                    success:function(response){
                        window.history.pushState(null,"ព័ត៌មានលំអិតនៃមាត្រា","<?php echo $this->getPath(array("page"=>"articleDetails")); ?>/id:"+articleId);
                        $(".cntn-overlay").show();
                        $(".msg-cntn").html(response);
                        $(".msg-h").slideDown();
                        //$(".msg-h").css("display")!="none"?$(".msg-h").slideUp():$(".msg-h").slideDown();
                    }
                });
            });

            function searchArticle(){
                window.location.href="<?php echo $this->getPath(array("page"=>"search")); ?>/s:"+$(".sec_searchbox").val();
            }
            $(".sec_searchbox").keydown(function(e){
                if(e.keyCode==13){
                    searchArticle();
                }
            });
            $(".sec_btnsearch").click(function(){
                searchArticle();
            });

        });
    </script>
</head>
<body style="background: #FAFAFA;" >
    <div class="pk-book-search" style="padding: 10px ; margin: 80px auto 0px auto;" >
        <div class="sec_search_h" style="border-botto: 1px solid #CCC; box-shadow: 0px 0px 2px #AAA; width: 688px; position: relative; display: block; float: none; margin: 0px auto; " >
            <input type="text" style=" width: 618px; " class="sec_searchbox " name="sec_searchbox" value="<?php echo @$search; ?>" placeholder="សូមបញ្ចូល ចំណងជើង ឬ អត្ថន័យ នៃ មាត្រា  ដើម្បីស្វែងរក..." />
            <div class="sec_btnsearch fa fa-search" style="background: #0075cf;" ></div>
        </div>
        <div class="search-result" style="background: rgba(250,250,250,0.4); padding: 20px 1% 5px 1%; width: 98%; margin: 10px auto;" >លទ្ធផលនៃការស្វែងរក <span style="color: #9859B6; font-size: 16px;  " ><?php echo count($articles); ?></span> មាត្រា ៖</div>
    </div>
    <div class="pk-blk pk-search-blk" style="text-align: center; position: relative; float: left; width: 100%; margin: 40px 0px 60px 0px; " >
        <?php
        $books=array(1=>"ក្រមរដ្ឋប្បវេណី",2=>"ក្រមនីតិវិធីរដ្ឋប្បវេណី",3=>"ក្រមព្រហ្មទណ្ឌ",4=>"ក្រមនីតិវិធីព្រហ្មទណ្ឌ");
        foreach( $articles AS $index => $article ){ ?>
        <div class="search-item" id="<?php echo $article->bid.",".$article->id; ?>">
            <div class="matra-no" ><?php echo $article->aid; ?></div>
            <div class="matra-title" ><?php echo $article->title; ?></div>
            <div class="matra-book" ><?php echo isset($books[$article->bid])?$books[$article->bid]:"មិនមានឈ្មោះ"; ?></div>
        </div>
        <?php } ?>
    </div>
    <div class="pk-ft" >
        <div class="pk-purpose" >សម្រាប់ការប្រើប្រាស់ជាសាធារណៈ<div style="position: relative; float: right; display: inline-block; top: -3px; padding: 0px 10px; cursor: pointer; color: #02508c; " >អំពី<span style="font-size:16px; color: #D770AD; font-weight: bold" >ខ្ញុំ</span></div></div>
    </div>

    <div class="cntn-overlay" ></div>
    <div class="msg-h" >
        <div class="msg-cntn" >?</div>
        <div class="msg-btns" >
            <input type="button" value="បិទ" class="msg-cancel" />
            <input type="button" value="បាទ" class="msg-ok" style="display: none; " />
        </div>
    </div>

    <div class="pk-blk" style=" border-botto: 1px solid #CCC; box-shadow: 0px 0px 2px #AAA; height: 40px; padding: 5px 0px 4px 0px; position: fixed; left: 0; top: 0; float: left; background: #FAFAFA; " >
        <div class="site-name" >ផ្កាឈូក.<span style="font-size: 12px; " >org</span></div>
        <div class="pk-logo" style=" border:none; border-bottom: 1px solid #CCC;" ></div>
        <div class="fb-like" data-width="100" data-layout="button" data-action="like" data-show-faces="false" data-share="false" style="position: absolute; color: #0075cf; top: 15px; right: 10px; " ></div>
    </div>
</body>
</html>