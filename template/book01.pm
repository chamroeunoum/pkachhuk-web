<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=1124" />
    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ទំព័រដើម : ផ្កាឈូក</title>
    <?php
    Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","book01"));
    Import::js(array("jquery","jqueryui","printelementmin","selectize","script","book01"));
    ?>
    <script type="text/javascript" >

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1011781982167975',
                xfbml      : true,
                version    : 'v2.0'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $(document).ready(function(){

            function searchArticle(){
                window.location.href="<?php echo $this->getPath(array("page"=>"search")); ?>/s:"+$(".sec_searchbox").val();
            }
            $(".sec_searchbox").keydown(function(e){
                if(e.keyCode==13){
                    searchArticle();
                }
            });
            $(".sec_btnsearch").click(function(){
                searchArticle();
            });
        });
    </script>
</head>
<body>
    <div class="book-ads" >
        <div class="welcome-message" style="display: block; width: 100%; " >សូមស្វាគមន៍, សូមជ្រើសរើសច្បាប់ដែលអ្នកពេញចិត្ត</div>
        <div class="pk-book-search" style="padding: 10px ; margin: 530px auto 0px auto;" >
            <div class="sec_search_h" style="width: 688px; position: relative; display: block; float: none; margin: 0px auto; box-shadow: 0px 0px 1px #999; " >
                <input type="text" style=" width: 603px; padding: 2px 80px 2px 5px;  " class="sec_searchbox " name="sec_searchbox" value="" placeholder="សូមបញ្ចូល ចំណងជើង ឬ អត្ថន័យ នៃ មាត្រា  ដើម្បីស្វែងរក..." />
                <div class="sec_btnsearch fa fa-search" ></div>
            </div>
        </div>
    </div>

    <div class="pk-blk pk-mcntn" ></div>

    <div class="pk-ft" >
        <div class="pk-purpose" >អ៊ីមែលអ្នកគ្រប់គ្រងគេហទំព័រ៖ admin@pkachhuk.com ឬ info@pkachhuk.com</div>
    </div>

    <div class="pk-blk" style=" height: 40px; padding: 5px 0px 4px 0px; position: fixed; left: 0; top: 0; float: left; background: #FAFAFA; " >
        <div class="site-name" style="display: none; " >ផ្កាឈូក.<span style="font-size: 12px; " >org</span></div>
        <div class="pk-logo" ></div>
        <div class="fb-like" data-width="100" data-layout="button" data-action="like" data-show-faces="false" data-share="false" style="position: absolute; color: #0075cf; top: 15px; right: 10px; " ></div>
    </div>
</body>
</html>