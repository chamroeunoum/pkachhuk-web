<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="icon" href="<?php echo Import::link("img/pk-64.png");?>" type="image/jpg" />
    <title>ទំព័រដើម : ផ្កាឈូក</title>
    <?php
    Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","home","main"));
    Import::js(array("jquery","jqueryui","printelementmin","selectize","script","home"));
    ?>
</head>
<body>
    <div class="pk-blk" style="text-align: center;" ><div class="pk-blogo" ></div></div>
    <div class="pk-blk" >
        <div class="pk-blk-center" style="font-family: KHMUOLLI; font-size: 40px; " >ផ្កាឈូក</div>
        <div class="pk-blk-center" style="font-family: KHMUOLLI; font-size: 30px; " >សូមជ្រើសរើសសៀវភៅ</div>
    </div>
    <div class="indx_1_728x90" >
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Index_1_728x90 -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-4781007779447186"
             data-ad-slot="2999953555"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <div class="list-books" >
        <div class="book-item" onclick="window.location='<?php echo $this->getPath(array("civil"=>"index")); ?>';" >ច្បាប់<br/>រដ្ឋប្បវេណី</div>
        <div class="book-item" onclick="window.location='<?php echo $this->getPath(array("criminal"=>"index")); ?>';" >ច្បាប់<br/>ព្រហ្មទណ្ឌ</div>
        <div class="book-item" style="color: #AAA;" >ច្បាប់<br/>ភូមិបាល</div>
        <div class="book-item" style="color: #AAA;" >និតិ<br/>រដ្ឋប្បវេណី</div>
        <div class="book-item" style="color: #AAA;" >និតិ<br/>ព្រហ្មទណ្ឌ</div>
    </div>
    <div class="indx_1_728x90" >
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Index_1_728x90 -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-4781007779447186"
             data-ad-slot="2999953555"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <div class="pk-ft" >
        <div class="pk-purpose" >សម្រាប់ការប្រើប្រាស់ជាសាធារណៈ</div>
    </div>
</body>
</html>