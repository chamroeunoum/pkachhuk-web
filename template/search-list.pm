<div class="pk-blk " style="width: 19%; padding: 0 0.5%; display: none; " >
    <div class="pk-mcntn-left" >
        <!-- Books -->
        <div class="pk-book-active" ><div class="fa fa-book" ></div>ក្រមរដ្ឋប្បវេណី</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>និតិវិធីក្រមរដ្ឋប្បវេណី</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ក្រមព្រហ្មទណ្ឌ</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>និតិវិធីក្រមព្រហ្មទណ្ឌ</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់រដ្ឋធម្មនុញ្ញ</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់ស្ដីការប្រឆាំងនឹងភេរវកម្ម</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់សហគ្រាសពាណិជ្ជកម្ម</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់សិទ្ធិអ្នកនិពន្ធ និងសិទ្ធិប្រហាក់ប្រហែល</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់ព្រៃឈើ</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់ចរាចរណ៍ផ្លូវគោក</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់ការគ្រប់គ្រង និងការធ្វើអាជីវកម្មធនធានរ៉ែ </div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់ការបង្រ្កាបអំពើជួញដូរមនុស្ស និង អំពើធ្វើអាជីវកម្មផ្លូវភេទ</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់ស្ដីពីសារពើពន្ធ</div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់វិសោធនកម្ម នៃច្បាប់ស្ដីពីសារពើពន្ធ </div>
        <div class="pk-book" ><div class="fa fa-book" ></div>ច្បាប់ស្ដីពីអគ្គីសនី នៃព្រះរាជាណាចក្រកម្ពុជា </div>
    </div>
</div>
<div class="pk-blk blk-80pc" style="width: 99%; padding: 0 0.5%; height: 800px; display: block; " >
    <div class="pk-mcntn-right" >
        <div class="pk-book-search" style="padding: 10px ;" >
            <div class="sec_search_h" style="width: 688px; position: relative; display: block; float: left; " >
                <input type="text" style=" width: 618px; " class="sec_searchbox " name="sec_searchbox" value="" placeholder="សូមបញ្ចូល ចំណងជើង ឬ អត្ថន័យ នៃ មាត្រា  ដើម្បីស្វែងរក..." />
                <div class="sec_btnsearch fa fa-search" ></div>
            </div>
            <div class="search-result" >លទ្ធផលនៃការស្វែងរក <span style="color: #9859B6; font-size: 16px;  " >1392</span> មាត្រា ៖</div>
        </div>
        <div class="pk-blk pk-matra-list" >
            <div class="pk-blk pk-matra-item" style="width: 100%;" >
                <div class="pk-matra-item-no" >មាត្រា ១៖</div>
                <div class="pk-matra-item-title" >ច្បាប់ទូទៅនៃនីតិឯកជន</div>
                <div class="pk-matra-item-cntn" >ក្រមនេះ បញ្ញត្ដអំពីគោលការណ៍ទូទៅដែលទាក់ទងទៅនឹងទំនាក់ទំនងគតិយុត្ដផ្នែក
                    រដ្ឋប្បវេណី ។ បញ្ញត្ដិនៃក្រមរដ្ឋប្បវេណីនេះ ត្រូវយកមកអនុវត្ដ ចំពោះទំនាក់ទំនងទ្រព្យ
                    សម្បត្ដិ និង ទំនាក់ទំនងញាតិ លើកលែងតែមានកំណត់ផ្សេង នៅក្នុងច្បាប់ពិសេស ។</div>
                <div class="pk-social facebook-share fa fa-pinterest" style="color: #cd0a0a; " title="ចែករំលែកទៅកាន់មិត្តតាមរយៈ ​Pinterest" ></div>
                <div class="pk-social twitter-share fa fa-twitter" style="color: #0075cf; " title="ចែករំលែកទៅកាន់មិត្តតាមរយៈ​ Twitter" ></div>
                <div class="pk-social google-plus-share fa fa-google-plus" style="color: #cd0a0a; " title="ចែករំលែកទៅកាន់មិត្តតាមរយៈ Google Plus" ></div>
                <div class="pk-social pinterest-share fa fa-facebook" style="color: #0075cf; " title="ចែករំលែកទៅកាន់មិត្តតាមរយៈ facebook" ></div>
            </div>
        </div>
    </div>
    <div class="ads-block" style="margin: 10px 0px 40px 0px; " >
        <div class="indx_1_728x90" style="background: #FAFAFA; box-shadow: 0px 0px 1px #CCC; width: 728px; height: 90px; margin: 0px auto; display: block; overflow: hidden; position: relative;" ></div>
    </div>
</div>