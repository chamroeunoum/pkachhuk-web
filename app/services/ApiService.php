<?php
/**
 * Created by PhpStorm.
 * User: chamroeunoum
 * Date: 01/04/16
 * Time: 23:24
 */

class ApiService extends Service {
    function books(){
        $civilModel = $this->loadModel('civil');
        $books = $civilModel->getAllBooks();

        echo json_encode($books);
    }

    function bookStructure($bookId){
        $bookId = $this->get("id");
        if(!isset($bookId)) $bookId = 1;

        /* get book structure */
        $civilModel = $this->loadModel('civil');
        $civilBook = $civilModel->getBookStructure(1);

        echo json_encode($civilBook);
    }
}