<?php
/**
 * User: chamroeunoum
 * Date: 11/4/15
 * Time: 13:43
 */

class CriminalService extends Service {
    public function index() {
        $fil = DATA_FOLDER."criminal_code.dat";

        $p = $this->get("p")!=null&&$this->get("p")!==""?$this->get("p"):1;
        $start = (($p-1)*50) ;
        $stop = $start+49 ;
        $articles = array();
        $total_articles = 0;
        if( file_exists($fil) ){
            $fil = file_get_contents($fil);
            $temp_articles = explode("\r",$fil);
            $total_articles = count($temp_articles);
            $total_pages = ceil($total_articles/50);
            foreach( $temp_articles AS $index => $article ){
                if( $start<=$index&&$index<=$stop ){
                    list($id,$content)=explode("#",$article);
                    $articles[$id]=$content;
                }
            }
        }
        $this->set(array(
            "mnu"=>"criminal",
            "current"=>$p,
            "total_pages"=>$total_pages,
            "total_articles"=>$total_articles,
            "prev"=>$p<=1?1:$p-1,
            "next"=>$p>=$total_pages?$total_pages:$p+1,
            "articles"=>$articles
        ));

        $this->template("criminal_articles");
    }
}