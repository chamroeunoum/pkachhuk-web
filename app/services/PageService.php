<?php
/**
 * Created by PhpStorm.
 * User: chamroeunoum
 * Date: 22/5/15
 * Time: 23:24
 */

class PageService extends Service {
    /**
     *
     */
    public function index(){
        $civil = $this->loadModel("Civil");
        $civilBook = $civil->getBookStructure(1);
        $civilProcedureBook = $civil->getBookStructure(2);
        $criminalBook = $civil->getBookStructure(3);
        $criminalProcedureBook = $civil->getBookStructure(4);
        $this->set(array(
            "civilBook" => $civilBook,
            "civilProcedureBook" => $civilProcedureBook,
            "criminalBook" => $criminalBook,
            "criminalProcedureBook" => $criminalProcedureBook
        ));
        $this->template("book01");
    }
    public function search(){
        $civil = $this->loadModel("Civil");
        $search = $this->get("s");
        $articles = array();
        if($search!==NULL&&$search!="") {
            $articles = $civil->getArticles($search);
        }
        $this->set(array(
            "search"=>$search,
            "articles"=>$articles
        ));
        $this->template("search2");
    }
    public function articleDetails(){
        $civil = $this->loadModel("Civil");
        $bid = $this->get("bid");
        $id = $this->get("id");
        $search = $this->get("s");
        $article = $civil->getArticleDetails($bid,$id,$search);
        $relatedArticles = $civil->getBookArticles($article->bid->id,$article->kid->id,$article->mid->id,$article->cid->id,$article->pid->id,$article->sid->id);
        $this->set(array(
            "bid"=>$bid,
            "id"=>$id,
            "article"=>$article,
            "relatedArticles"=>$relatedArticles
        ));
        $this->template("detailsArticle");
    }
    public function readBook(){
        $civil = $this->loadModel("Civil");
        $bid=$this->get("id")!==NULL?$this->get("id"):1;
        $book = $civil->getBookStructure($bid);
        $this->set(array(
            "bid"=>$bid,
            "book"=>$book
        ));
        $this->template("book");
    }
    public function readArticle(){
        $civil = $this->loadModel("Civil");
        $bid = $this->get("bid");
        $id = $this->get("id");
        $search = $this->get("s");
        $article = $civil->getArticleDetails($bid,$id,$search);
        $this->set(array(
            "article"=>$article
        ));
        $this->template("readArticleAjax");
    }
    // data response
    public function bookStructure(){
        $civil = $this->loadModel("Civil");
        $book = $civil->getBookStructure(2);
        print_r( $book );
    }
    public function getArticlesByCategory(){
        $civil = $this->loadModel("Civil");
        list($bid,$kid,$mid,$cid,$pid,$sid)=explode(",",$this->get("id"));
        $articles = $civil->getBookArticles($bid,$kid,$mid,$cid,$pid,$sid);
        $this->block("listArticles",array("articles"=>$articles));
    }
    /*
    public function dataIntegration() {

        $civil = $this->loadModel("Civil");
        echo '
        <!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=1124" />
<title>ទំព័រដើម : ផ្កាឈូក</title>
<?php
Import::css(array("calendartheme","jquery-ui","font-awesome-min","selectize-default","pk-block","style","book01"));
Import::js(array("jquery","jqueryui","printelementmin","selectize","script","book01"));
?>
</head>
<body>
        ';
        $file = DATA_FOLDER."criminalprocedure.txt";
        if(file_exists($file)){
            $content = file_get_contents($file);
            $kunties = array();
            $bookid=4;
            $i=0;
            $pf = "";
            $cf = "";
            $string = "";
            $ki=$mi=$ci=$pi=$si=$ai=0;
            echo "Processing start ...";
            while($i<strlen($content)){
                $ch = $content[$i];
                if($ch=="@"||$ch=="&"||$ch=="$"||$ch=="*"||$ch=="#"||$ch=="%"){
                    switch($pf){
                        case "@":
                            $ki++;
                            list($number,$title)=explode("\n",$string);
                            $kunties[$ki]["bid"]=$bookid;
                            $kunties[$ki]["number"] = $number ;
                            $kunties[$ki]["name"] = $title ;
                            $kunties[$ki]["id"]=$civil->addKunty($bookid,$number,$title);
                            $mi=0;
                            $ci=0;
                            $cf = "@" ;
                            break;
                        case "%":
                            $mi++;
                            list($number,$title)=explode("\n",$string);
                            $kunties[$ki]["matika"][$mi]=array(
                                "bid"=>$bookid,
                                "kid"=>$kunties[$ki]["id"],
                                "number"=>$number,
                                "name"=>$title,
                                "id"=>$civil->addMatika($bookid,$kunties[$ki]["id"],$number,$title)
                            );
                            $ci=0;
                            $cf = "%" ;
                            break;
                        case "&":
                            $ci++;
                            list($number,$title)=explode("\n",$string);
                            $kunties[$ki]["chapters"][$ci]=array(
                                "bid"=>$bookid,
                                "kid"=>$kunties[$ki]["id"],
                                "mid"=>$kunties[$ki]["matika"][$mi]["id"],
                                "number"=>$number,
                                "name"=>$title,
                                "id"=>$civil->addChapter($bookid,$kunties[$ki]["id"],$kunties[$ki]["matika"][$mi]["id"],$number,$title)
                            );
                            $pi=0;
                            $cf = "&" ;
                            break;
                        case "$":
                            $pi++;
                            list($number,$title)=explode("\n",$string);
                            $kunties[$ki]["chapters"][$ci]["parts"][$pi]=array(
                                "bid"=>$bookid,
                                "kid"=>$kunties[$ki]["id"],
                                "mid"=>$kunties[$ki]["matika"][$mi]["id"],
                                "cid"=>$kunties[$ki]["chapters"][$ci]["id"],
                                "number"=>$number,
                                "name"=>$title,
                                "id"=>$civil->addPart($bookid,$kunties[$ki]["id"],$kunties[$ki]["matika"][$mi]["id"],$kunties[$ki]["chapters"][$ci]["id"],$number,$title)
                            );
                            $si=0;
                            $cf = "$" ;
                            break;
                        case "*":
                            $si++;
                            list($number,$title)=explode("\n",$string);
                            $kunties[$ki]["chapters"][$ci]["parts"][$pi]["sections"][$si]=array(
                                "bid"=>$bookid,
                                "kid"=>$kunties[$ki]["id"],
                                "mid"=>$kunties[$ki]["matika"][$mi]["id"],
                                "cid"=>$kunties[$ki]["chapters"][$ci]["id"],
                                "pid"=>$kunties[$ki]["chapters"][$ci]["parts"][$pi]["id"],
                                "number"=>$number,
                                "name"=>$title,
                                "id"=>$civil->addSection($bookid,$kunties[$ki]["id"],$kunties[$ki]["matika"][$mi]["id"],$kunties[$ki]["chapters"][$ci]["id"],$kunties[$ki]["chapters"][$ci]["parts"][$pi]["id"],$number,$title)
                            );
                            $cf = "*" ;
                            break;
                        case "#":
                            $ai++;
                            list($number,$title)=explode("._",substr($string,0,strpos($string,"\n",0)));
                            $meaning = str_replace(array("\n"),"",substr($string,strpos($string,"\n",0)));
                            $article =array(
                                "bid"=>$bookid,
                                "kid"=>$kunties[$ki]["id"],
                                "mid"=>$kunties[$ki]["matika"][$mi]["id"],
                                "cid"=>$kunties[$ki]["chapters"][$ci]["id"],
                                "pid"=>$kunties[$ki]["chapters"][$ci]["parts"][$pi]["id"],
                                "sid"=>$kunties[$ki]["chapters"][$ci]["parts"][$pi]["sections"][$si]["id"],
                                "number"=>$number,
                                "title"=>$title,
                                "meaning"=>$meaning,
                                "id"=>$civil->addArticle($bookid,$kunties[$ki]["id"],$kunties[$ki]["matika"][$mi]["id"],$kunties[$ki]["chapters"][$ci]["id"],$kunties[$ki]["chapters"][$ci]["parts"][$pi]["id"],$kunties[$ki]["chapters"][$ci]["parts"][$pi]["sections"][$si]["id"],$number,$title,$meaning)
                            );
                            switch($cf){
                                case "@":
                                    $kunties[$ki]["articles"][$ai]=$article;
                                    break;
                                case "&":
                                    $kunties[$ki]["chapters"][$ci]["articles"][$ai]=$article;
                                    break;
                                case "$":
                                    $kunties[$ki]["chapters"][$ci]["parts"][$pi]["articles"][$ai]=$article;
                                    break;
                                case "*":
                                    $kunties[$ki]["chapters"][$ci]["parts"][$pi]["sections"][$si]["articles"][$ai]=$article;
                                    break;
                            }
                            break;
                    }
                    $string="";
                    $pf=$ch;
                    $i++;
                    continue;
                }
                $string.=$ch;
                if($i>=strlen($content)-1){
                    $ai++;
                    list($number,$title)=explode("._",substr($string,0,strpos($string,"\n",0)));
                    $meaning = str_replace(array("\n"),"",substr($string,strpos($string,"\n",0)));
                    $article =array(
                        "bid"=>$bookid,
                        "kid"=>$kunties[$ki]["id"],
                        "mid"=>$kunties[$ki]["matika"][$mi]["id"],
                        "cid"=>$kunties[$ki]["chapters"][$ci]["id"],
                        "pid"=>$kunties[$ki]["chapters"][$ci]["parts"][$pi]["id"],
                        "sid"=>$kunties[$ki]["chapters"][$ci]["parts"][$pi]["sections"][$si]["id"],
                        "number"=>$number,
                        "title"=>$title,
                        "meaning"=>$meaning,
                        "id"=>$civil->addArticle($bookid,$kunties[$ki]["id"],$kunties[$ki]["matika"][$mi]["id"],$kunties[$ki]["chapters"][$ci]["id"],$kunties[$ki]["chapters"][$ci]["parts"][$pi]["id"],$kunties[$ki]["chapters"][$ci]["parts"][$pi]["sections"][$si]["id"],$number,$title,$meaning)
                    );
                    switch($cf){
                        case "@":
                            $kunties[$ki]["articles"][$ai]=$article;
                            break;
                        case "%":
                            $kunties[$ki]["matika"][$mi]["articles"][$ai]=$article;
                            break;
                        case "&":
                            $kunties[$ki]["chapters"][$ci]["articles"][$ai]=$article;
                            break;
                        case "$":
                            $kunties[$ki]["chapters"][$ci]["parts"][$pi]["articles"][$ai]=$article;
                            break;
                        case "*":
                            $kunties[$ki]["chapters"][$ci]["parts"][$pi]["sections"][$si]["articles"][$ai]=$article;
                            break;
                    }
                }
                $i++;
            };

            echo $i . " Finished" ;
            // print_r( $kunties );
        }

        echo '
        </body>
</html>
        ';
        // $this->template("book01");
    }
    */
    public function getArticle($statement){
        $articles = explode("#",$statement);
        print_r( $articles );
    }
}