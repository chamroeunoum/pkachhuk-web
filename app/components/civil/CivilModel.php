<?php
/**
 * User: chamroeunoum
 * Date: 12/6/15
 * Time: 09:53
 */

class CivilModel extends Model {
    public function addKunty($bid,$number,$title){
        $query = "INSERT INTO Kunties SET bid='$bid', kid='$number', title='$title', created='".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ; " ;
        $result = $this->query( $query );
        if(is_object($result))return $this->getInsertId();
        return null ;
    }
    public function addMatika($bid,$kid,$number,$title){
        $query = "INSERT INTO Matikas SET bid='$bid',  kid='$kid', mid='$number', title='$title', created='".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ; " ;
        $result = $this->query( $query );
        if(is_object($result))return $this->getInsertId();
        return null ;
    }
    public function addChapter($bid,$kid,$mid,$number,$title){
        $query = "INSERT INTO Chapters SET bid='$bid',  kid='$kid', mid='$mid', cid='$number', title='$title', created='".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ; " ;
        $result = $this->query( $query );
        if(is_object($result))return $this->getInsertId();
        return null ;
    }
    public function addPart($bid,$kid,$mid,$cid,$number,$title){
        $query = "INSERT INTO Parts SET bid='$bid',  pid='$number', kid='$kid', mid='$mid', cid='$cid', title='$title', created='".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ; " ;
        $result = $this->query( $query );
        if(is_object($result))return $this->getInsertId();
        return null ;
    }
    public function addSection($bid,$kid,$mid,$cid,$pid,$number,$title){
        $query = "INSERT INTO Sections SET bid='$bid',  kid='$kid', mid='$mid', cid='$cid',pid='$pid', sid='$number', title='$title', created='".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ; " ;
        $result = $this->query( $query );
        if(is_object($result))return $this->getInsertId();
        return null ;
    }
    public function addArticle($bid,$kid,$mid,$cid,$pid,$sid,$number,$title,$meaning){
        $query = "INSERT INTO Articles SET bid='$bid', kid='$kid', mid='$mid', cid='$cid',pid='$pid', sid='$sid', aid='$number', title='$title', meaning='$meaning', created='".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ; " ;
        $result = $this->query( $query );
        if(is_object($result))return $this->getInsertId();
        return null ;
    }
    public function getArticles($search){
        $query = "SELECT * FROM Articles " ;

        $terms = explode(" ",$search);
        $searchColumns = array("aid","title","meaning");
        $tempSearchFields=array();
        $caseMatchesCount=array();
        $tempquery = array() ;
        $tempmatches = array() ;
        foreach($searchColumns AS $field ){
            foreach($terms as $term ){
                $tempSearchFields[]= " `$field` LIKE '%".Basic::stringEscape(trim($term))."%' " ;
                $caseMatchesCount[]= " CASE WHEN `$field` LIKE '%".Basic::stringEscape(trim($term))."%' THEN 1 ELSE 0 END " ;
            }
            $tempquery[] = " ( " . implode( " OR " , $tempSearchFields ) . " ) ";
            $tempmatches[] = implode(" + ", $caseMatchesCount );
            $tempSearchFields=array();
            $caseMatchesCount=array();
        }
        $matchQuery = "( " . implode( " + " , $tempmatches ) . " ) AS numMatches ";
        $query = str_replace("SELECT *","SELECT *, ". $matchQuery ,$query);
        $query .= " WHERE ( " . implode(" OR ", $tempquery ) . " ) " ;
        $query .= " ORDER BY numMatches DESC " ;

        $result = $this->query( $query );
        $rows = array();
        while($row=$result->fetch(PDO::FETCH_OBJ)){
            foreach($terms as $term ){
                $row->title = str_replace($term,"<span class='search-term-highlight'>$term</span>",$row->title);
            }
            $rows[$row->id] = $row;
        }
        return $rows;
    }
    public function getArticle($bid,$id){
        $query = "SELECT * FROM Articles WHERE bid='".htmlentities($bid,ENT_QUOTES)."' AND id='".htmlentities($id,ENT_QUOTES)."' " ;
        $result = $this->query($query);
        return $result->fetch(PDO::FETCH_OBJ);
    }
    public function getArticleDetails($bid,$id,$search){
        $query = "SELECT * FROM Articles WHERE bid='".htmlentities($bid,ENT_QUOTES)."' AND id='".htmlentities($id,ENT_QUOTES)."' " ;
        $result = $this->query($query);
        $article = $result->fetch(PDO::FETCH_OBJ);

        $terms = explode(" ",$search);
        foreach($terms as $term ){
            $article->title = str_replace($term,"<span class='search-term-highlight'>$term</span>",$article->title);
            $article->meaning = str_replace($term,"<span class='search-term-highlight'>$term</span>",$article->meaning);
        }

        $query = "SELECT id,title,color FROM Books WHERE id='$article->bid' LIMIT 0,1 ;";
        $result = $this->query($query);
        $article->bid = $result->fetch(PDO::FETCH_OBJ);

        $query = "SELECT id,kid,title FROM Kunties WHERE id='$article->kid' LIMIT 0,1 ;";
        $result = $this->query($query);
        $article->kid = $result->fetch(PDO::FETCH_OBJ);

        $query = "SELECT id,mid,title FROM Matikas WHERE id='$article->mid' LIMIT 0,1 ;";
        $result = $this->query($query);
        $article->mid = $result->fetch(PDO::FETCH_OBJ);

        $query = "SELECT id,cid,title FROM Chapters WHERE id='$article->cid' LIMIT 0,1 ;";
        $result = $this->query($query);
        $article->cid = $result->fetch(PDO::FETCH_OBJ);

        $query = "SELECT id,pid,title FROM Parts WHERE id='$article->pid' LIMIT 0,1 ;";
        $result = $this->query($query);
        $article->pid = $result->fetch(PDO::FETCH_OBJ);

        $query = "SELECT id,sid,title FROM Sections WHERE id='$article->sid' LIMIT 0,1 ;";
        $result = $this->query($query);
        $article->sid = $result->fetch(PDO::FETCH_OBJ);

        return $article;
    }
    public function getBookStructure($bid){
        $query = "SELECT id,title,color FROM Books WHERE id='".htmlentities($bid,ENT_QUOTES)."' ";
        $result = $this->query($query);
        $book = $result->fetch(PDO::FETCH_ASSOC);
        $query = "SELECT id,kid,title FROM Kunties WHERE bid='".$book["id"]."' ;" ;
        $kresult = $this->query($query);
        while($kunty=$kresult->fetch(PDO::FETCH_ASSOC)){
            $book["kunties"][$kunty["id"]]=$kunty;
            $query = "SELECT id,mid,title FROM Matikas WHERE kid='".$kunty["id"]."' ;" ;
            $mresult = $this->query($query);
            $matikaHelper = false;
            while($matika=$mresult->fetch(PDO::FETCH_ASSOC)){
                $matikaHelper = true;
                $book["kunties"][$kunty["id"]]["matikas"][$matika["id"]]=$matika;
                $query = "SELECT id,cid,title FROM Chapters WHERE mid='".$matika["id"]."' ;" ;
                $cresult = $this->query($query);
                while($chapter=$cresult->fetch(PDO::FETCH_ASSOC)){
                    $book["kunties"][$kunty["id"]]["matikas"][$matika["id"]]["chapters"][$chapter["id"]]=$chapter;
                    $query = "SELECT id,pid,title FROM Parts WHERE cid='".$chapter["id"]."' ;" ;
                    $presult = $this->query($query);
                    while($part=$presult->fetch(PDO::FETCH_ASSOC)){
                        $book["kunties"][$kunty["id"]]["matikas"][$matika["id"]]["chapters"][$chapter["id"]]["parts"][$part["id"]]=$part;
                        $query = "SELECT id,sid,title FROM Sections WHERE pid='".$part["id"]."' ;" ;
                        $sresult = $this->query($query);
                        while($section=$sresult->fetch(PDO::FETCH_ASSOC)){
                            $book["kunties"][$kunty["id"]]["matikas"][$matika["id"]]["chapters"][$chapter["id"]]["parts"][$part["id"]]["sections"][$section["id"]]=$section;
                        }
                    }
                }
            }
            if(!$matikaHelper){
                $query = "SELECT id,cid,title FROM Chapters WHERE kid='".$kunty["id"]."' ;" ;
                $cresult = $this->query($query);
                while($chapter=$cresult->fetch(PDO::FETCH_ASSOC)){
                    $book["kunties"][$kunty["id"]]["chapters"][$chapter["id"]]=$chapter;
                    $query = "SELECT id,pid,title FROM Parts WHERE cid='".$chapter["id"]."' ;" ;
                    $presult = $this->query($query);
                    while($part=$presult->fetch(PDO::FETCH_ASSOC)){
                        $book["kunties"][$kunty["id"]]["chapters"][$chapter["id"]]["parts"][$part["id"]]=$part;
                        $query = "SELECT id,sid,title FROM Sections WHERE pid='".$part["id"]."' ;" ;
                        $sresult = $this->query($query);
                        while($section=$sresult->fetch(PDO::FETCH_ASSOC)){
                            $book["kunties"][$kunty["id"]]["chapters"][$chapter["id"]]["parts"][$part["id"]]["sections"][$section["id"]]=$section;
                        }
                    }
                }
            }
        }
        return $book;
    }
    public function getBookArticles($bid,$kid,$mid,$cid,$pid,$sid){
        $query = "SELECT id,aid,title, meaning FROM Articles ";
        is_numeric($bid)&&$bid>-1?$conditions[] = " bid='$bid' ":false;
        is_numeric($kid)&&$kid>-1?$conditions[] = " kid='$kid' ":false;
        is_numeric($mid)&&$mid>-1?$conditions[] = " mid='$mid' ":false;
        is_numeric($cid)&&$cid>-1?$conditions[] = " cid='$cid' ":false;
        is_numeric($pid)&&$pid>-1?$conditions[] = " pid='$pid' ":false;
        is_numeric($sid)&&$sid>-1?$conditions[] = " sid='$sid' ":false;
        $query .= " WHERE " . implode(" AND ",$conditions) . " ; ";
        $result = $this->query( $query );
        $rows = array();
        while($row=$result->fetch(PDO::FETCH_OBJ)){
            $rows[$row->id] = $row;
        }
        return $rows;
    }
    public function getAllBooks(){
        $query = "SELECT id, title, description, cover FROM Books ORDER BY id";
        $result = $this->query( $query );
        $rows = array();
        while($row=$result->fetch(PDO::FETCH_OBJ)){
            $row->cover = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/web/img/' . $row->cover;
            $rows[] = $row;
        }
        return $rows;
    }
}